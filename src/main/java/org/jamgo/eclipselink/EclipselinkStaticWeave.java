package org.jamgo.eclipselink;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.Converter;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.eclipse.persistence.logging.AbstractSessionLog;
import org.eclipse.persistence.tools.weaving.jpa.StaticWeaveProcessor;
import org.gradle.api.DefaultTask;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.compile.JavaCompile;
import org.gradle.util.GFileUtils;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

public class EclipselinkStaticWeave extends DefaultTask {

	private final static String DEFAULT_LOG_LEVEL = "ERROR";

	private final Logger logger = Logging.getLogger(this.getClass());

	private String[] basePackages;
	private String source;
	private String target;
	private String logLevel;

	public String[] getBasePackages() {
		return this.basePackages;
	}

	public void setBasePackages(String[] basePackages) {
		this.basePackages = basePackages;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return this.target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getLogLevel() {
		return this.logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	@TaskAction
	void performEclipselinkStaticWeaveTask() throws DocumentException, IOException, URISyntaxException {
		if (this.source == null) {
			this.source = this.getCompiledClassesDir().getPath();
		}
		if (this.target == null) {
			this.target = this.getCompiledClassesDir().getPath();
		}

		URL[] classPath = null;
		classPath = this.getClassPath();
		ClassLoader classLoader = new URLClassLoader(classPath, Thread.currentThread().getContextClassLoader());

		this.logger.info("Starting weaving task.");
		List<String> entityClassNames = null;
		entityClassNames = this.getEntityClassNames(classPath);
		this.logger.debug("entityClassNames = " + entityClassNames);
		this.createTemporaryPersistenceInfo(entityClassNames);
		this.performWeaving(classLoader);
	}

	private URI getCompiledClassesDir() {
		return this.getProject().getTasks().withType(JavaCompile.class).getByName("compileJava").getDestinationDir().toURI();
	}

	private URL[] getClassPath() throws MalformedURLException {
		List<URL> urls = new ArrayList<>();
		urls.add(this.getCompiledClassesDir().toURL());
		for (File eachFile : this.getProject().getConfigurations().findByName("compile").getFiles()) {
			urls.add(eachFile.toURI().toURL());
		}
		return urls.toArray(new URL[urls.size()]);
	}

	private List<String> getEntityClassNames(URL[] classPath) {
		this.logger.debug("basePackages = " + Arrays.toString(this.basePackages));
		this.logger.debug("classPath = " + Arrays.toString(classPath));
		final FastClasspathScanner classpathScanner = new FastClasspathScanner(this.basePackages).overrideClasspath((Object[]) classPath);
		final ScanResult scanResult = classpathScanner.scan();
		this.logger.debug(scanResult.getNamesOfAllClasses().toString());
		return scanResult.getNamesOfClassesWithAnnotationsAnyOf(Entity.class, MappedSuperclass.class, Embeddable.class, Converter.class);
	}

	private void createTemporaryPersistenceInfo(List<String> entityClassNames) throws DocumentException, IOException {
		SAXReader reader = new SAXReader();
		Document persistenceXmlDocument = reader.read(Resources.asCharSource(Resources.getResource("empty-persistence.xml"), Charsets.UTF_8).openStream());
		Node persistenceUnitNode = persistenceXmlDocument.selectSingleNode("//*[name()='persistence-unit']");
		this.logger.debug(persistenceUnitNode.getPath());
		if (persistenceUnitNode != null) {
			this.logger.debug("persistence-unit found");
			for (String eachEntityClassName : entityClassNames) {
				Element classElement = ((Element) persistenceUnitNode).addElement("class");
				classElement.setText(eachEntityClassName);
			}
		}
		GFileUtils.mkdirs(new File(this.getTemporaryDir().getAbsolutePath() + "/META-INF"));
		File temporaryPersistenceInfoFile = new File(this.getTemporaryDir().getAbsolutePath() + "/META-INF/persistence.xml");

		Writer writer = new FileWriter(temporaryPersistenceInfoFile);
		OutputFormat format = OutputFormat.createPrettyPrint();
		XMLWriter xmlWriter = new XMLWriter(writer, format);
		xmlWriter.write(persistenceXmlDocument);
		xmlWriter.close();
	}

	private void performWeaving(ClassLoader classLoader) throws URISyntaxException, IOException {
		this.logger.debug("source = " + this.source);
		this.logger.debug("target = " + this.target);
		this.logger.debug("classpath = " + this.getProject().getConfigurations().findByName("compile").getAsPath());
		final StaticWeaveProcessor weaveProcessor = new StaticWeaveProcessor(this.source, this.target);
		weaveProcessor.setPersistenceInfo(this.getTemporaryDir().toURI().toURL());
		weaveProcessor.setClassLoader(classLoader);
		weaveProcessor.setLog(new PrintWriter(System.out));
		weaveProcessor.setLogLevel(AbstractSessionLog.translateStringToLoggingLevel(Optional.ofNullable(this.logLevel).orElse(DEFAULT_LOG_LEVEL).toUpperCase()));
		weaveProcessor.performWeaving();
	}
}
