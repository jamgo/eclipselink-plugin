package org.jamgo.eclipselink;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.gradle.api.DefaultTask;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.compile.JavaCompile;
import org.springframework.orm.jpa.persistenceunit.DefaultPersistenceUnitManager;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;

import com.google.common.base.Joiner;

public class EclipselinkDdlGeneration extends DefaultTask {

	private final Logger logger = Logging.getLogger(this.getClass());

	private String[] basePackages;
	private String[] databaseProductNames;
	private String logLevel;

	public String[] getBasePackages() {
		return this.basePackages;
	}

	public void setBasePackages(String[] basePackages) {
		this.basePackages = basePackages;
	}

	public String[] getDatabaseProductNames() {
		return this.databaseProductNames;
	}

	public void setDatabaseProductNames(String[] databaseProductNames) {
		this.databaseProductNames = databaseProductNames;
	}

	public String getLogLevel() {
		return this.logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	@TaskAction
	void performEclipselinkDdlGenerationTask() throws MalformedURLException {
		final Thread thread = Thread.currentThread();
		final ClassLoader currentClassLoader = thread.getContextClassLoader();
		try {
			ClassLoader classLoader = new URLClassLoader(this.getClassPath(), Thread.currentThread().getContextClassLoader());
			thread.setContextClassLoader(classLoader);
			this.performDllGeneration();
		} finally {
			thread.setContextClassLoader(currentClassLoader);
		}
	}

	private Map<String, Object> buildProperties(String databaseProductName) {
		final Map<String, Object> properties = new TreeMap<>();

		properties.put(PersistenceUnitProperties.SCHEMA_GENERATION_DATABASE_ACTION, PersistenceUnitProperties.SCHEMA_GENERATION_NONE_ACTION);
		properties.put(PersistenceUnitProperties.SCHEMA_GENERATION_SCRIPTS_ACTION, PersistenceUnitProperties.SCHEMA_GENERATION_CREATE_ACTION);
		properties.put(PersistenceUnitProperties.SCHEMA_GENERATION_CREATE_SOURCE, PersistenceUnitProperties.SCHEMA_GENERATION_METADATA_SOURCE);
		properties.put(PersistenceUnitProperties.SCHEMA_GENERATION_SCRIPTS_CREATE_TARGET, this.getTargetFile(databaseProductName));
		properties.put(PersistenceUnitProperties.SCHEMA_DATABASE_PRODUCT_NAME, databaseProductName);
		properties.put(PersistenceUnitProperties.WEAVING, "false");

		return properties;
	}

	private File getTargetFile(String databaseProductName) {
		return new File(this.getProject().getBuildDir(), databaseProductName + "-ddl.sql");
	}

	private URI getCompiledClassesDir() {
		return this.getProject().getTasks().withType(JavaCompile.class).getByName("compileJava").getDestinationDir().toURI();
	}

	private URL[] getClassPath() throws MalformedURLException {
		List<URL> urls = new ArrayList<>();
		urls.add(this.getCompiledClassesDir().toURL());
		for (File eachFile : this.getProject().getConfigurations().findByName("compile").getFiles()) {
			urls.add(eachFile.toURI().toURL());
		}
		return urls.toArray(new URL[urls.size()]);
	}

	private void performDllGeneration() throws MalformedURLException {
		for (String eachDatabaseProductName : this.databaseProductNames) {
			String[] allBasePackages = this.getBasePackages();
			this.logger.info("Using base packages " + Joiner.on(", ").join(allBasePackages));
			final PersistenceProvider provider = new PersistenceProvider();
			final DefaultPersistenceUnitManager manager = new DefaultPersistenceUnitManager();
			manager.setDefaultPersistenceUnitRootLocation(null);
			manager.setDefaultPersistenceUnitName("default");
			manager.setPackagesToScan(allBasePackages);
			manager.setPersistenceXmlLocations(new String[0]);
			manager.afterPropertiesSet();

			final MutablePersistenceUnitInfo puInfo = (MutablePersistenceUnitInfo) manager.obtainDefaultPersistenceUnitInfo();
			puInfo.setPersistenceProviderPackageName(provider.getClass().getName());
			puInfo.setPersistenceUnitRootUrl(new URL("http://foo.bar"));
			this.logger.info("Managed classes found: " + puInfo.getManagedClassNames().size());
			this.logger.debug("Managed class names:\n -> " + Joiner.on("\n -> ").join(puInfo.getManagedClassNames()));
			final Map<String, Object> properties = this.buildProperties(eachDatabaseProductName);
			puInfo.getProperties().putAll(properties);
			provider.generateSchema(puInfo, properties);
		}
	}
}
