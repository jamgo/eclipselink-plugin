package org.jamgo.eclipselink;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

public class EclipselinkGradlePlugin implements Plugin<Project> {

	@Override
	public void apply(Project project) {
		Task compileJavaTask = project.getTasks().findByName("compileJava");
		Task staticWeaveTask = project.getTasks().create("eclipselinkStaticWeave", EclipselinkStaticWeave.class);
		staticWeaveTask.dependsOn(compileJavaTask);
		Task ddlGenerationTask = project.getTasks().create("eclipselinkDdlGeneration", EclipselinkDdlGeneration.class);
		ddlGenerationTask.dependsOn(compileJavaTask);
	}

}
